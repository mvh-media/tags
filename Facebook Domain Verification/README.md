# Facebook Domain Verification
Domein verificatie script t.b.v. Google Tag Manager.

## Variables
| Name              | Variable      |
| ----------------- | ------------- |
| Verification key  | %KEY%         |
